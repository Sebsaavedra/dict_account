# -*- coding: utf-8 -*-
from odoo import models, fields, api


class subasignacion(models.Model):
    _name = 'subasignacion.subasignacion'
    _inherit = ['mail.thread', 'mail.activity.mixin', 'portal.mixin', 'account.account']
    _description = "Sub-Asignación Cuentas Contables"

    cuenta_id = fields.Many2one('account.account', track_visibility='onchange', index=True)
    asignacion_id = fields.Many2one('asignacion.asignacion', track_visibility='onchange', index=True)
    nombre_subasignacion = fields.Char(string="Sub-Asignación", track_visibility='onchange', index=True)
    codigo_subasignacion = fields.Char(string="Código Sub-Asignación", track_visibility='onchange', index=True)

    message_attachment_count = fields.Integer(string="Conteo de archivos adjuntos", track_visibility='onchange', index=True)
    message_channel_ids = fields.Many2many('mail.channel', string="Seguidores (Canales)", track_visibility='onchange', index=True)
    message_follower_ids = fields.One2many('mail.followers', 'res_id', track_visibility='onchange', index=True)
    message_ids = fields.One2many('mail.message', 'res_id', track_visibility='onchange', index=True)
    message_main_attachment_id = fields.Many2one('ir.attachment', track_visibility='onchange', index=True)


class asignacion(models.Model):
    _name = 'asignacion.asignacion'
    _inherit = ['mail.thread', 'mail.activity.mixin', 'portal.mixin', 'account.account']
    _description = "Asignación Cuentas Contables"


    item_id = fields.Many2one('item.item', track_visibility='onchange', index=True)
    nombre_asignacion = fields.Char(string="Asignación", track_visibility='onchange', index=True)
    codigo_asignacion = fields.Char(string="Código Asignación", track_visibility='onchange', index=True)

    message_attachment_count = fields.Integer(string="Conteo de archivos adjuntos", track_visibility='onchange', index=True)
    message_channel_ids = fields.Many2many('mail.channel', string="Seguidores (Canales)", track_visibility='onchange', index=True)
    message_follower_ids = fields.One2many('mail.followers', 'res_id', track_visibility='onchange', index=True)
    message_ids = fields.One2many('mail.message', 'res_id', track_visibility='onchange', index=True)
    message_main_attachment_id = fields.Many2one('ir.attachment', track_visibility='onchange', index=True)


class item(models.Model):
    _name = 'item.item'
    _inherit = ['mail.thread', 'mail.activity.mixin', 'portal.mixin', 'account.account']
    _description = "Item Cuentas Contables"

    subtitulo_id = fields.Many2one('subtitulo.subtitulo', track_visibility='onchange', index=True)
    nombre_item = fields.Char(string="Item", track_visibility='onchange', index=True)
    codigo_item = fields.Char(string="Código Item", track_visibility='onchange', index=True)

    message_attachment_count = fields.Integer(string="Conteo de archivos adjuntos", track_visibility='onchange', index=True)
    message_channel_ids = fields.Many2many('mail.channel', string="Seguidores (Canales)", track_visibility='onchange', index=True)
    message_follower_ids = fields.One2many('mail.followers', 'res_id', track_visibility='onchange', index=True)
    message_ids = fields.One2many('mail.message', 'res_id', track_visibility='onchange', index=True)
    message_main_attachment_id = fields.Many2one('ir.attachment', track_visibility='onchange', index=True)

class subtitulo(models.Model):
    _name = 'subtitulo.subtitulo'
    #_rec_name = 'name_subtitulo'
    _inherit = ['mail.thread', 'mail.activity.mixin', 'portal.mixin','account.account']
    _description = "Subtitulo Cuentas Contables"

    name = fields.Char(compute='_compute_fields_combination', required=True, track_visibility='onchange', readonly=True, index=True,)
    code = fields.Char(compute='_compute_code_combination', required=True, track_visibility='onchange', readonly=True, index=True,)
    user_type_id = fields.Char(compute='_compute_code_combination', track_visibility='onchange', required='0', index=True,)
    nombre_subtitulo = fields.Char(string="Subtitulo", track_visibility='onchange', index=True, required=True)
    codigo_subtitulo = fields.Char(string="Código Subtitulo", track_visibility='onchange', index=True, required=True )
    #name_subtitulo = fields.Char(string='Name', compute='_compute_fields_combination')

    message_attachment_count = fields.Integer(string="Conteo de archivos adjuntos", track_visibility='onchange', index=True)
    message_channel_ids = fields.Many2many('mail.channel', string="Seguidores (Canales)", track_visibility='onchange', index=True)
    message_follower_ids = fields.One2many('mail.followers', 'res_id', track_visibility='onchange', index=True)
    message_ids = fields.One2many('mail.message', 'res_id', track_visibility='onchange', index=True)
    message_main_attachment_id = fields.Many2one('ir.attachment', track_visibility='onchange', index=True)

    @api.depends('nombre_subtitulo', 'codigo_subtitulo')
    def _compute_fields_combination(self):
        for i in self:
            i.name_subtitulo = str(i.nombre_subtitulo) + '-' + str(i.codigo_subtitulo)
            return i.name_subtitulo


    @api.depends('codigo_subtitulo')
    def _compute_code_combination(self):
        for x in self:
            x.name_code = str(i.codigo_subtitulo)
            return x.name_code

@api.multi
@api.returns('mail.message', lambda value: value.id)
def message_post(self, **kwargs):
    if self.env.context.get('mark_rfq_as_sent'):
        self.filtered(lambda o: o.state == 'draft').write({'state': 'sent'})
    return super(PurchaseOrder, self.with_context(mail_post_autofollow=True)).message_post(**kwargs)

